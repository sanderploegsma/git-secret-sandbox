Playing around with [git-secret](https://git-secret.io/).

## Setting up

- Install `git-secret` using the [installation instructions](https://git-secret.io/installation)
- Create a symlink to install the pre-commit hook: `ln -s .githooks/pre-commit .git/hooks/pre-commit`. This hook will make sure that all secrets are encrypted and added to git before committing.

## How to add or update secrets

To add a file as a secret, run:

    git secret add <file>

To encrypt the secret, run:

    git secret hide -m

## How to add someone to the secret store

Let's suppose that Alice already has access to the secrets, and Bob wants access. 

Bob should first export their public GPG key and send it to Alice:

```shell
gpg --armor --export bob@email.id > public_key_bob.txt
```

Alice then imports Bob's public key in her key ring:

```shell
gpg --import public_key_bob.txt
```

After this, Alice can add Bob to the secrets:

```shell
git secret tell bob@email.id
```

For Bob to be able to decrypt the existing secrets, they have to be re-encrypted by Alice:

```shell
git secret reveal && git secret hide -d
```

Finally, after Alice has committed and pushed the changes, Bob can read the secrets:

```shell
git secret cat my-secret-file.txt
```